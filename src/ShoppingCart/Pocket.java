import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;


public class Pocket {
   /**
    * The RandomAccessFile of the pocket file
    */
   private RandomAccessFile file;

   /**
    * Creates a Pocket object
    * 
    * A Pocket object interfaces with the pocket RandomAccessFile.
    */
    public Pocket () throws Exception {
        this.file = new RandomAccessFile(new File("pocket.txt"), "rw");
    }

   /**
    * Adds a product to the pocket. 
    *
    * @param  product           product name to add to the pocket (e.g. "car")
    */
    public void addProduct(String product) throws Exception {
        FileChannel Filchan = this.file.getChannel();
        FileLock lockAttempt = null;
        lockAttempt = Filchan.tryLock();
        if (lockAttempt == null) { //See if File lock status has moved from null signifying lock
            System.out.println("Could not lock pocket");
            throw new Exception("Lock integrity could not be assured canceling sale");
        }

          this.file.seek(this.file.length());
          this.file.writeBytes(product+'\n'); 

        lockAttempt.release();
    }

   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
        this.file.close();
    }
}
