import java.util.Scanner;
import java.util.Date;
import java.io.*;  
import java.net.*; 

public class ShoppingCart  {

  	public static void main(String[] args) throws Exception {

		try{
	 		int port = 8888;
	 		ServerSocket serversocket=new ServerSocket(port);  
	   		System.out.println("ShoppingCart program is running on port " + port);
	   		System.out.println("Waiting for connections from clients");
	  		while (true){
		 		Socket socket=serversocket.accept();//establishes connection 
		   		System.out.println("A new customer is connected!");  
				OutputStreamWriter outputstreamwriter= new  OutputStreamWriter(socket.getOutputStream());
				BufferedReader bufferreader= new BufferedReader(new InputStreamReader(socket.getInputStream()));
				Thread thread=new Thread(new ServerThread(socket,outputstreamwriter,bufferreader));
		   		thread.start();
			}
		}catch(IOException ex){
			System.out.println(ex.getMessage());
		}
	}

	public static  class ServerThread  extends Exception implements Runnable  {
		final Socket socket;
		final BufferedReader bufferreader; 
		final OutputStreamWriter outputstreamwriter; 

	   	public ServerThread(Socket socket, OutputStreamWriter outputstreamwriter, BufferedReader bufferreader){
		    this.socket = socket;
		    this.outputstreamwriter=outputstreamwriter;
		    this.bufferreader=bufferreader;
		}

	   	public void run() {

		   	try{
				Scanner input = new Scanner(System.in);
				PrintWriter out=new PrintWriter(outputstreamwriter);
			   	Wallet wallet = new Wallet();
			   	int balance = wallet.getBalance();
				//Add your name after SS-LBS!!! 
				out.println("Welcome to SS-LBS's ShoppingCart. The time now is " + (new Date()).toString());
				out.println("Your balance: " + balance+ " credits");
				out.println("Please select your product: ");
			    
				for (Object element :Store.asString() ) {
					out.println(element);
			   	}
				
				out.println("What do you want to buy, type e.g., pen?");
			    	out.flush();
			    	String product = bufferreader.readLine(); 
			    	int price = Store.getPrice(product);
				
                wallet.safeWithdraw(price);

                    out.println("DEBUG withdrawnAmount PA has value " + wallet.withdrawnAmount);
                    
                    if (price != wallet.withdrawnAmount){
                        out.println("Price != withdrawn amount cannot purchase");
                    }else if (price == 0){
                        out.println("Price = 0 not real purcahse ");
                    }else{
                        Pocket pocket = new Pocket();
                        pocket.addProduct(product); 
                    }
                    //Pocket pocket = new Pocket();
                    //pocket.addProduct(product);

                    if (price == 0){
                       out.println("The item: '" + product + "' is not present in the store, Goodbye"); 
                    }else if (price != wallet.withdrawnAmount){
                        out.println("No purchase of '" + product + "'");
                    }else{
                        out.println("You have sucessfully purchased a '" + product + "'");
                    }
				    //out.println("You have sucessfully purchased a '" + product + "'");

                    if (price == 0){
                       	out.println("Your balance remains: " + wallet.getBalance()+ " credits"); 
                    }else{
                        out.println("Your new balance: " + wallet.getBalance()+ " credits");
                    }
				    //out.println("Your new balance: " + wallet.getBalance()+ " credits");
			            out.flush();

			 	socket.close();
			}catch(Exception ex){
		   		System.out.println(ex.getMessage());
		    }
		}
	}
}
