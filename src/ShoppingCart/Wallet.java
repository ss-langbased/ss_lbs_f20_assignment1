import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;

public class Wallet {
   /**
    * The RandomAccessFile of the wallet file
    */  
   private RandomAccessFile file;

   /**
    * Creates a Wallet object
    *
    * A Wallet object interfaces with the wallet RandomAccessFile
    */
    public Wallet () throws Exception {
	this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
    }

   /**
    * Gets the wallet balance. 
    *
    * @return                   The content of the wallet file as an integer
    */
    public int getBalance() throws IOException {
	this.file.seek(0);
	return Integer.parseInt(this.file.readLine());
    }

   /**
    * Sets a new balance in the wallet
    *
    * @param  newBalance          new balance to write in the wallet
    */
    public void setBalance(int newBalance) throws Exception {
	this.file.setLength(0);
	String str = new Integer(newBalance).toString()+'\n'; 
	this.file.writeBytes(str); 
    }

   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
	this.file.close();
    }


    /**
    *Assignment 1 Addition
    *Used instead of setBalance to check balance in wallet
    */

    public int withdrawnAmount = 0;
    
    public int safeWithdraw(int valueToWithdraw) throws Exception{
        FileChannel Filchan = this.file.getChannel();
        FileLock lockAttempt = null;
        lockAttempt = Filchan.tryLock();
        if (lockAttempt == null) { //See if File lock status has moved from null signifying lock
            System.out.println("Could not lock Wallet");
            throw new Exception("Lock integrity could not be assured AmountWithdrew:" + withdrawnAmount);
        }
        
        int bal = getBalance();
        
        if (bal >= valueToWithdraw){
            int temp = bal - valueToWithdraw; //New Wallet amount
            this.setBalance(temp); //Set Wallet
            withdrawnAmount = valueToWithdraw;
            lockAttempt.release(); //Release lock for legit reuse
            return(withdrawnAmount); //Returning withdrawn value
        } else if (valueToWithdraw == 0) { // Address if trying to buy something with value of 0
            withdrawnAmount = 0;
            lockAttempt.release();
            throw new Exception("Trying to buy something with value of 0. No action taken " + withdrawnAmount);
            //return(temp); //Nothing withdrawn
        } else { // Known flaw to take remaining funds
            withdrawnAmount = getBalance(); //Balance before funds pulled
            this.setBalance(0); //Set Wallet as all value is taken
            lockAttempt.release();
            //throw new Exception("Not enough money Taking all funds " + withdrawnAmount);
            return(withdrawnAmount); //Known flaw to pull all Wallet Balance
        }
    }

    public void safeDeposit (int valueToDeposit) throws Exception{
        int cur_bal = getBalance(); //Gets the current balance of wallet at time of updating
        if (cur_bal >= valueToDeposit){ //The Balance in Wallet is greater then the safe return, we shouldn't be here
            throw new Exception("Trying to add back too much, oops");
        } else {
            this.setBalance(valueToDeposit); //Returning funds
        }
    }
}
