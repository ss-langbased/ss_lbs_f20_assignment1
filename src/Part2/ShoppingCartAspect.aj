import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.io.*;  
import java.net.*; 

public aspect ShoppingCartAspect{
	
    pointcut safeWithdraw(int price):
    	call(* Wallet.safeWithdraw(int)) && args(price);


    before(int price): safeWithdraw(int) && args(price) {
    	System.out.println("Before SW");

    		try{
				Wallet wallet = new Wallet();
				int balance = wallet.getBalance();

				System.out.println("AOP Balance " + balance + " Price " + price);

				if (balance < price){

				System.out.println("Not Enough Money for Item ");

    			JFrame parent = new JFrame();
    			JOptionPane.showMessageDialog(parent, "Not Enough Money for Item"); 

				}

    		}catch(Exception ex){
    			System.out.println(ex.getMessage());
    		}

    }

    //other code

    after(int price) returning (int withdrawnAmmount):
    safeWithdraw(price){
    	
    	System.out.println("DEBUG AFTER Aspect : " + (new Date()).toString() + " tHE price IS " + price + " tHE withdrawnAmount IS " + withdrawnAmmount); 

    	if (withdrawnAmmount < price){
    		try{
    			
				Wallet wallet = new Wallet();
	    		wallet.safeDeposit(withdrawnAmmount);

	    		JFrame parent = new JFrame();
    			JOptionPane.showMessageDialog(parent, "Returning Money to Wallet. Not enough for purchase"); 

    		}catch(Exception ex){
    			System.out.println(ex.getMessage());
    		}
    		
    	}
    }

}
