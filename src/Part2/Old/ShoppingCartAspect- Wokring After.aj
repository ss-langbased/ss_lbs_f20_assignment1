import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.io.*;  
import java.net.*; 

public aspect ShoppingCartAspect{
	
    pointcut safeWithdraw(int price):
    	call(* Wallet.safeWithdraw(int)) && args(price);

    before(int balance): call(* Wallet.setBalance(int)) && args(balance){
    	System.out.println("Aspect- Balance Correct:  Before-balance-callWallet.setBal: tHE BALANCE IS " + balance + " tHE price IS ");

    	JFrame parent = new JFrame();
    	JOptionPane.showMessageDialog(parent, "TestMessage"); 
    }

    before(int balance, int price): safeWithdraw(price) && args(balance){
    	System.out.println("Aspect -Balance Before-balance, price-safeWithdraw: tHE BALANCE IS " + balance + " tHE price IS " + price);

    	JFrame parent = new JFrame();
    	JOptionPane.showMessageDialog(parent, "TestMessage"); 
    }

    before(): safeWithdraw(int) {
    	System.out.println("Before SW");
    }

    //other code

    after(int price) returning (int withdrawnAmmount):
    safeWithdraw(price){
    	
    	System.out.println("DEBUG AFTER Aspect : " + (new Date()).toString() + " tHE price IS " + price + " tHE withdrawnAmount IS " + withdrawnAmmount); 

    	if (withdrawnAmmount < price){
    		try{
    			
				Wallet wallet = new Wallet();
	    		wallet.safeDeposit(withdrawnAmmount);

    		}catch(Exception ex){
    			System.out.println(ex.getMessage());
    		}
    		
    	}
    }

}
